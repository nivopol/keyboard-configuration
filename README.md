## Introduction

My personal keyboard configuration files.

Disclaimer: personal computer system is a laptop using Debian 12 / X11 / XFCE. Professionnal are respectively a tower using company-approved Ubuntu 20.04 default components and a laptop using company-full-locked up-to-date Windows 10. These are the only system I test theses files on. Your mileage may vary.

**.kbd** are KMonad files corresponding to various physical keyboards with some comments on the software.

**custom** is the Xkb symbol file I use to add [Optimot](https://optimot.fr) base keyboard layout in my personal computer X11 system, following process describe by Peter Hutterer in  [this new Xkb feature](https://gitlab.freedesktop.org/xkeyboard-config/xkeyboard-config/-/merge_requests/189). Read also the referenced issue 257 and the link it provides for a full picture of the problem.

**.xkb** files are keymap files to load keyboard configurations through Xkbcomp.

**.XCompose** files are X Compose composition file to be placed in the user directory.

**YAML** and **TOML** files are input files for the Kalamine driver generator.

**politbureau** directory contains my "production ready" .xkb files, the relatively stable drivers I use at work.

### Custom file for Optimot

This file is based on the official Optimot 1.6 ISO Linux X driver with the following changes.

1. Optimot driver was at the time only a X keymap, so I extracted the symbol part.
1. I can’t define custom key types without patching X11 (actually, it should be possible on some modern platforms, but I wasn’t aware at the time and my systems are old) and I really do not care about having a "shortcut layer" where Ctrl-à is magically transformed as Ctrl-q, so I converted Optimot 6-layer key types in classical Xkb 4-layer key types.
1. I use a hacky .XCompose configuration to create a "qu/Qu" key, so I changed the <AD10> key layer 1 and 2 to <yen> and <paragraph> key symbols, acting as the access keys of the relevant composition sequences. Optimot ISO being angle-mod ready, I changed the irrelevant <AB05> key from a secondary backspace to a vanilla "q/Q" key.

The custom file itself is not enough to have a fully functional Optimot. It’s necessary to use the provided .XCompose from the relevant driver package for the custom complex dead key mapping, as Optimot basically redefines itself all dead keys behavior, without using the system locale one.

### Keymap files

**backup.xkb** is an Xkbcomp export of my system Azerty driver from the beginning. In case something goes awfully wrong.

### Compose files

I don’t put here the Optimot Compose file at the moment as I use it stock (could add it if I’m the one proposing some changes upstream).

I defined the "qu/Qu" key as part of my try on Erglace layout, so the `erglace.XCompose` contains only the needed sequence. This Compose configuration is not compatible out-of-the-box with my Ubuntu 20.04 due to GTK+ 3.0 limitation: no strings allowed as output, only single characters. This limitation could be bypassed by changing input method (framework). For instance, Emacs coming with its built-in ibus.el interface has no such problem. At the moment I use XIM, and no application seems to mind (Granted Eclipse gives a warning about flickering display, but I don’t notice anything).

#### Multiple Compose files

Switching between Compose configuration on Linux is a lot trickier than layout switching. Compose configuration was not thought to be associated with keyboard layout and so is loaded once and for all at X application startup, wich could be the whole desktop environment so logout/login is my current way to do it reliably. Some input method configuration will use a cache (GTK and Ibus at least). You could need to remove the cache file to take into account your changes.

Easiest I just symlink the Compose file I want to use to `~/.XCompose`.

Another way is to have a master .XCompose file with multiple inclusion directive in it and to comment whatever file you do not want to be loaded at the moment. Beware of interaction between files and multiple inclusions.


### Erglace

#### Erglace 0.1 on yaml sources

Erglace-original.yaml is Lysquid's Erglace, derivative of Erg-EAU, derivative of Nuclear Squid’s Ergo-L, in the condition where it was when I begin to take interest in learning it.

Erglace is a 1dk-family layout: a custom dead-key acts as a one-time AltGr.

Erglace.yaml is to work on my custom Erglace variant based on 0.1:
1. Use angle-mod, so the low-left keys are shifted on left to use ISO 105th key and let Qwerty B free to use.
2. Update to the latest Ergo-L project AltGr layer (Ergo-L 0.99).
3. Switch Q and F keys, then put a Qu/qu key instead of Q, using .XCompose from 2 characters I don’t use (Yen and paragraph symbol).
4. Affect single Q to the Qwerty B key instead of the Qwerty/Azerty ISO 105th < and >.

#### Erglace 0.3 on toml sources

Due to Kalamine improvements, source format was changed from yaml to toml before the right consonants carousel marking the work on Erglace 0.2.

I reconvened the trunk version with Erglace 0.3 validation. Other changes are Ergo‑L 0.7 LTS symbol layer (AltGr and alpha), comeback of the `.:` and `,;` punctuation keys and a 1dk layer revision to include same characters as Ergo‑L.

Kalamine newest version can now generate drivers with angle-mod-adapted keys, so putting Q on ISO key is enough for my variant.

### KMonad configuration

TODO: commit updated config and explain.

### Politbureau

As said, this directory is for driver I use at work, so they are non-experimental keymaps to be loaded with Xkbcomp, isolated to be kept safer from error and experiments.

Local version for each file is used at the office. Xrdp is used when working from home or other places, using Xrdp for remote connexion. Xrdp keyboard management uses XFree86 keycodes for compatibility reason with Xvnc.

Directory name is a joke, playing on similarity between *pilote*, French for driver, *bureau*, French for office, and politburo. As everyone knows, surely no terrible misunderstanding could arise from typos using new keyboard layouts every month.