# Licence de la disposition de clavier Optimot

La disposition Optimot, ses pilotes, et sa documentation, sont couverts par la licence [Creative Commons BY‑NC‑SA](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr).

Cette licence d’utilisation inclut le droit pour l’utilisateur particulier ou professionnel d’**utiliser**, **modifier**, et **diffuser** la disposition, ses pilotes, et sa documentation sous les conditions suivantes :
    • **Attribution (BY)** : Vous devez créditer la disposition « Optimot » et son auteur « Patrick Jamet », intégrer un lien vers le site web [optimot.fr](https://optimot.fr/), et indiquer si des modifications ont été effectuées à la disposition, ses pilotes ou sa documentation. Vous devez indiquer ces informations par tous les moyens raisonnables, sans toutefois suggérer que l'auteur vous soutient ou soutient la façon dont vous avez utilisé Optimot.
    • **Utilisation commerciale (NC)** :
    • Vous n'êtes pas autorisé à faire un usage commercial de tout ou partie de cette disposition, ses pilotes ou de sa documentation, ou de modifications de ceux-ci. Toute utilisation commerciale doit faire l’objet d’un accord préalable et écrit de l’auteur. 
    • L’utilisation et la distribution de la disposition sur les postes de travail de l’entreprise ou la société est cependant possible pour un usage exclusivement interne.
    • **Partage(SA)** : Dans le cas où vous effectuez des modifications, ou créez à partir de tout ou partie de la disposition Optimot, ses pilotes ou sa documentation, vous devez diffuser l'œuvre modifiée dans les mêmes conditions, c'est-à-dire avec la même licence avec laquelle l'œuvre originale a été diffusée.

**Pas de restrictions complémentaires** : Vous n'êtes pas autorisé à appliquer des conditions légales ou des mesures techniques qui restreindraient légalement autrui à utiliser la disposition Optimot, ses pilotes ou sa documentation dans les conditions décrites par la licence.

## Conditions complètes :
https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr

## Contact
Auteur :
	Patrick Jamet patrick.jamet@proton.me

Site internet :
	Documentation et pilotes : https://optimot.fr/
